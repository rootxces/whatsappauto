package com.example.rootxces.whatsappauto;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.os.Parcelable;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

public class NotificationAccessibilityService extends AccessibilityService{
    String[] packages = {"com.whatsapp"};

    protected void onServiceConnected() {
        Log.d("Tortuga", "AccessibilityService Connected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        info.packageNames = packages;
        info.notificationTimeout = 100;
        setServiceInfo(info);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent e) {
        Log.d("Tortuga","FML");
        if (e.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {
            Log.d("Tortuga","Recieved event");
            Parcelable data = e.getParcelableData();
            if (data instanceof Notification) {
                Log.d("Tortuga","Recieved notification");
                Notification notification = (Notification) data;
                Log.d("Tortuga","ticker: " + notification.tickerText);
                Log.d("Tortuga","icon: " + notification.icon);
                Log.d("Tortuga", "notification: "+ e.getText());
                String text = notification.tickerText.toString();
                String sender = text.substring(13);
                MainActivity.openWhatsApp(getApplicationContext(), sender);
            }
        }
    }

    @Override
    public void onInterrupt() {
        // TODO Auto-generated method stub
    }
}
